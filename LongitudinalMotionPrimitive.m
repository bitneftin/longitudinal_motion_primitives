% Abstract class for longitudinal motion primitives

%  #######################################################
%  #  _______   __  _____ ______ _______ _ ____   _____  #
%  #         \ |  |   ___|   ___|__   __| |    \ |       #
%  #          \|  |  __| |  __|    | |  | |     \|       #
%  #       |\     | |____| |       | |  | |   |\         #
%  #  _____| \____| _____|_|       |_|  |_|___| \______  #
%  #                                                     #
%  #######################################################

% Coded by Giammarco Valenti - version 2.0
% /// KEEP IT SIMPLE! \\\

classdef (Abstract) LongitudinalMotionPrimitive < handle
  % Abstract class for longitudinal motion primitives
  % For what is a motion primitive refer to paper "Artificial Co-Drivers as an Enabling Technology for Future Intelligent 
  % Vehicles and Transportation Systems" M. Da Lio et all.
  % This class represents a primitive of motion in an abstract fashion.
  % This class contains common property and methods for all the longitudinal motion primitives
  % This class also includes as subclass the "LongiJoined" which is a collector of other primitives

  properties (SetAccess = protected, Hidden = false)
    initial_state       % vector of initial states 
    final_state_data    % necessary data for the final desired state (it differ between primitives even in dimension)        
    T                   % Time horizon
  end
    
  methods

    %% Methods which returns the polinomia coefficients --------------------------
    % Coefficient order is in Matlab convention.
    function coeffi = xCoe( self )
      % Return primitive position polynomial coefficients
      coeffi = self.xCoefficients; % this property is primitive-specific
    end

    function coeffi = uCoe( self )
      % Return primitive velocity polynomial coefficients
      coeffi = polyder(self.xCoefficients); % this property is primitive-specific
    end

    function coeffi = aCoe( self )
      % Return primitive acceleration polynomial coefficients
      coeffi = polyder(polyder(self.xCoefficients)); % this property is primitive-specific
    end

    function coeffi = jCoe( self )
      % Return primitive jerk optimal input polynomial coefficients
      coeffi = polyder(polyder(polyder(self.xCoefficients))); % this property is primitive-specific
    end

    %%% --------------------------------------------------------------------------


    %% Direct evaluation of the primitives WITH ARGUMENT CHECK -----------------------------------------------
    function xValue = xCEval(self,t)
      % Evaluate position of the primitive in the t-instant with t in the set [0,T]
      if any(t>=0 & t<=self.T)                             % check if there is at least one value in the allowed range
        xValue = polyval(self.xCoe , t(t>=0 & t<=self.T)); % Plot in the allowed range from Polynomial
        if(any(~(t>=0 & t<=self.T)))                       % Plot check if some value were dropped
          warning('LongitudinalMotionPrimitive::xEval::Some value of the argument are ignored because they were out of the allowed range');
        end
      else                                                % if no value inside the range, return an error
        error('LongitudinalMotionPrimitive::xEval:: Argument values are all out of the allowed time range 0,T')
      end
    end

    function uValue = uCEval(self,t)
      % Evaluate velocity of the primitive in the t-instant with t in the set [0,T]
      if any(t>=0 & t<=self.T)                             % check if there is at least one value in the allowed range
        uValue = polyval(self.uCoe , t(t>=0 & t<=self.T)); % Plot in the allowed range from Polynomial
        if(any(~(t>=0 & t<=self.T)))                       % Plot check if some value were dropped
          warning('LongitudinalMotionPrimitive::uEval::Some value of the argument are ignored because they were out of the allowed range');
        end
      else                                                % if no value inside the range, return an error
        error('LongitudinalMotionPrimitive::uEval:: Argument values are all out of the allowed time range 0,T')
      end
    end

    function aValue = aCEval(self,t)
      % Evaluate acceleration of the primitive in the t-instant with t in the set [0,T]
      if any(t>=0 & t<=self.T)                             % check if there is at least one value in the allowed range
        aValue = polyval(self.aCoe , t(t>=0 & t<=self.T)); % Plot in the allowed range from Polynomial
        if(any(~(t>=0 & t<=self.T)))                       % Plot check if some value were dropped
          warning('LongitudinalMotionPrimitive::aEval::Some value of the argument are ignored because they were out of the allowed range');
        end
      else                                                % if no value inside the range, return an error
        error('LongitudinalMotionPrimitive::aEval:: Argument values are all out of the allowed time range 0,T')
      end
    end

    function jValue = jCEval(self,t)
      % Evaluate jerk optimal input of the primitive in the t-instant with t in the set [0,T]
      if any(t>=0 & t<=self.T)                             % check if there is at least one value in the allowed range
        jValue = polyval(self.jCoe , t(t>=0 & t<=self.T)); % Plot in the allowed range from Polynomial
        if(any(~(t>=0 & t<=self.T)))                       % Plot check if some value were dropped
          warning('LongitudinalMotionPrimitive::jEval::Some value of the argument are ignored because they were out of the allowed range');
        end
      else                                                % if no value inside the range, return an error
        error('LongitudinalMotionPrimitive::jEval:: Argument values are all out of the allowed time range 0,T')
      end
    end
    %%% --------------------------------------------------------------------------------------------------------------------------------


    %% Direct evaluation of the primitives WITHOUT ARGUMENT CHECK ----------------------------------------------------------------------
    % Those methods evaluate the primitives in times t.

    function xValue = xEval( self , t )
      % Evaluate position of the primitive in the t-instant
      % /!\ please make sure all the t values are in the allowed range [0,T] - no autocheck included 
        xValue = polyval(self.xCoe , t); % Plot in the allowed range from Polynomial
    end

    function uValue = uEval( self , t )
      % Evaluate velocity of the primitive in the t-instant
      % /!\ please check if all the t values are in the allowed range [0,T] - no autocheck included 
        uValue = polyval(self.uCoe , t); % Plot in the allowed range from Polynomial
    end

    function aValue = aEval( self , t )
      % Evaluate acceleration of the primitive in the t-instant
      % /!\ please make sure all the t values are in the allowed range [0,T] - no autocheck included 
        aValue = polyval(self.aCoe , t); % Plot in the allowed range from Polynomial
    end

    function jValue = jEval( self , t )
      % Evaluate jerk (optimal input) of the primitive in the t-instant
      % /!\ please make sure all the t values are in the allowed range [0,T] - no autocheck included 
        jValue = polyval(self.jCoe , t); % Plot in the allowed range from Polynomial
    end
    %%% --------------------------------------------------------------------------------------------------------------------------------

    %% Jerk in zero, this method will be overloaded for the single primitives
    function jZ = jZero(self)
      jZ = self.jEval(0);
    end
    % --------------------------------------------------------------------------------

    function finalstates = chainStates( self )
      % return [ v_final a_final ] to concatenate a new primitive
      finalstates = [ self.uEval(self.T) self.aEval(self.T) ];
    end
    

  end

  methods (Access = protected)
      core() 
    % Computes the main quentities required: it is allowed to run only into the constructor
  end
end