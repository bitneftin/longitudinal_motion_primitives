% Abstract class for joined longitudinal motion primitives

%  #######################################################
%  #  _______   __  _____ ______ _______ _ ____   _____  #
%  #         \ |  |   ___|   ___|__   __| |    \ |       #
%  #          \|  |  __| |  __|    | |  | |     \|       #
%  #       |\     | |____| |       | |  | |   |\         #
%  #  _____| \____| _____|_|       |_|  |_|___| \______  #
%  #                                                     #
%  #######################################################

% Coded by Giammarco Valenti - version 2.1 - beta
% /// KEEP IT SIMPLE! \\\

classdef LongiJoined < LongitudinalMotionPrimitive
	% class to join longitudinal motion primitives (LongitudinalMotionPrimitive instances)

	properties (SetAccess = protected, Hidden = false)
	    % initial_state       % Of the first primitive
        % final_state_data    % Of the last primitive
        % T                   % Total time horizon
		n  		         % number of primitives
		primitives_array % cell array of primitives
		Tall             % vector fo the final times
	end


	methods

		function self = LongiJoined( varargin )
			% LongiJoined( primitive1 , primitive2 , ... )
			if nargin < 1
				error('LongiJoined:: Not enough input arguments')
			elseif nargin >= 1
				self.primitives_array = cell(nargin,1); % preallocate
				for i = 1:nargin
					if isa(varargin{i},'LongitudinalMotionPrimitive')
						self.primitives_array{i} = varargin{i};
					else 
						error('LongiJoined:: Invalid type of some input arguments')
					end
				end
				self.core; % Compute the other quantites
			end
		end

		function num = np(self)
			num = self.n
		end

		%% Direct evaluation of the primitives 

	    function xValue = xEval(self,t)
	      %Evaluate position of the primitive in the t-instant with t in the set [0,T]
	      [ equivalent_t k ] = self.timeTrasposition(t);          % find equivalent t
	      xValue = zeros(size(equivalent_t));

	      % compute space offset
	      offsets = zeros(1,self.n+1); % first zero is necessary
	      for i = 1:self.n
	      	offsets(i+1) = offsets(i) + self.primitives_array{i}.xEval(self.primitives_array{i}.T);
	      end

	      % evaluate joined primitive
	      for i = 1:length(equivalent_t) % To be optimized with polynomial evaluation
	      	xValue(i) = offsets(k(i)) + self.primitives_array{k(i)}.xEval(equivalent_t(i));  % eval @ right t the right primitive
	      end
	  	end

	    function [ uValue cutTime ] = uEval(self,t)
	    % Evaluate velocity of the primitive in the t-instant with t in the set [0,T]
	    % cutTime is the time if it exceeds 
			  if any(t>=0 & t<=self.T)                             % check if there is at least one value in the allowed range
			  	t = t(t>=0 & t<=self.T);                           % allowed value of t - with the T of the Joined
			    [ equivalent_t k ] = self.timeTrasposition(t);          % find equivalent t
			    uValue = zeros(size(equivalent_t));
			    for i = 1:length(equivalent_t) % To be optimized with polynomial evaluation
			      uValue(i) = self.primitives_array{k(i)}.uEval(equivalent_t(i));  % eval @ right t the right primitive
			    end
		        if(any(~(t>=0 & t<=self.T)))                       % Plot check if some value were dropped
		          warning('LongitudinalMotionPrimitive::uEval::Some value of the argument are ignored because they were out of the allowed range');
		        end
		      else                                                % if no value inside the range, return an error
		        error('LongitudinalMotionPrimitive::uEval:: Argument values are all out of the allowed time range 0,T')
		      end
		      
		      cutTime = t; % return the time , if 0<t<T cutTime = input time
	  	end

	    function aValue = aEval(self,t)
	      %Evaluate acceleration of the primitive in the t-instant with t in the set [0,T]
	      [ equivalent_t k ] = self.timeTrasposition(t);          % find equivalent t
	      aValue = zeros(size(equivalent_t));
	      for i = 1:length(equivalent_t) % To be optimized with polynomial evaluation
	      	aValue(i) = self.primitives_array{k(i)}.aEval(equivalent_t(i));  % eval @ right t the right primitive
	      end
	  	end

	    function jValue = jEval(self,t)
	      %Evaluate jerk of the primitive in the t-instant with t in the set [0,T]
	      [ equivalent_t k ] = self.timeTrasposition(t);          % find equivalent t
	      jValue = zeros(size(equivalent_t));
	      for i = 1:length(equivalent_t) % To be optimized with polynomial evaluation
	      	jValue(i) = self.primitives_array{k(i)}.jEval(equivalent_t(i));  % eval @ right t the right primitive
	      end
	  	end

	  	% wrapper for jZero

	  	function jz = jZero(self)
	  		% call the proper jZero method of the first primitive
	  		jz = self.primitives_array{1}.jZero();
	  	end

	end

	methods (Access = protected)

    	function core(self)
			self.n 			      = length(self.primitives_array);
			time                  = 0;	
			% Take the last final state (its data)	
			self.final_state_data = self.primitives_array{self.n}.final_state_data;
			% Take the first initial state 
			self.initial_state    = self.primitives_array{1}.initial_state;
			% initialize vector of times
			self.Tall = zeros(1,self.n);
			for i=1:self.n 
				time = time + self.primitives_array{i}.T;
				self.Tall(i) = time;
			end
			self.T = time;		
		end

		function [ time, whichPrimitive ] = timeTrasposition(self,t)

			if(size(t,1) == 1)
				t = t.'; % transpose it if needed
			end

			[ row , k ]      = find( self.Tall >= t );  % find the right primitive

			[ ~   , IU , ~ ] = unique(row);             % remove multiple row solutions

			k = k(IU);  							    % index rightly

			offsets     = [ 0 self.Tall ];   % create offsets options with the first as zero
			offset      = offsets(k).';      % choose the right one

			% get the time of the joined path and remove the offset to evaluate the k-th primitive
			time           = t - offset;

			whichPrimitive = k;


			% collapse in final T a set +/- 10^-9 ( for numerical reason )
			% TEMPORARY FIX
			time(self.primitives_array{end}.T - 1e-09 <= time & time <= self.primitives_array{end}.T + 1e-09)...
			= self.primitives_array{end}.T;
		end

  	end

% IMPORTANT NOTE: POSITION MUST BE CUMULATIVE. SINCE EVERY PRIMITIVE STARTS FROM ZERO

end
