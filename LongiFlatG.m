% Class of primitives - Flat (zero jerk and constant speed)

%  #######################################################
%  #  _______   __  _____ ______ _______ _ ____   _____  #
%  #         \ |  |   ___|   ___|__   __| |    \ |       #
%  #          \|  |  __| |  __|    | |  | |     \|       #
%  #       |\     | |____| |       | |  | |   |\         #
%  #  _____| \____| _____|_|       |_|  |_|___| \______  #
%  #                                                     #
%  #######################################################

% Coded by Giammarco Valenti - version 1.0
% /// KEEP IT SIMPLE! \\\

classdef LongiFlatG < LongitudinalMotionPrimitive
  % Class for longitudinal flat primitives (zero jerk and constant speed)
  % For what is a motion primitive refer to paper "Artificial Co-Drivers as an Enabling Technology for Future Intelligent 
  % Vehicles and Transportation Systems" M. Da Lio et all.
  % This class represents a simple flat velocity primitive: mainly aimed to extend serious primitives

  % Initial state: velocity ONLY. the acceleration is deleted and replaced with 0.

  properties (SetAccess = protected, Hidden = false)
    % initial_state       % [ V_0 0 ] Acceleration is null in this primitive
    % final_state_data    % [ V_0 0 ] Same as the initial ones (constant velocity)     
    % T                   % Time horizon
    xCoefficients         % coefficients of the primitive polynomial x(t) (in descent power order (MATLAB convention))
  end

  methods 

    function self = LongiFlatG(varargin)
      % Constructor of a flat primitive. Examples of usage:
      % Primitive = LongiFlatG( v_0 ).................... a_0 = 0; T = 5;
      % Primitive = LongiFlatG( v_0 , T )................ a_0 = 0;
      % Primitive = LongiFlatG( [v_0 a_0 ] )............. a_0 = 0; T = 5;
      % Primitive = LongiFlatG( [v_0 a_0 ] , T).......... a_0 = 0;
      % Initial acceleration will be discarded and set as zero.

      % Defaults
      self.T = 5;


      % input check and parsing
      if nargin < 1
        error('LongiFlatG:: bad constructor usage: Not enough input arguments')
      elseif nargin == 1
        if (length(varargin{1}) <= 2) % check size
          self.initial_state = [ varargin{1} 0 ];
        else
          error('LongiFlatG:: bad constructor usage: wrong size of some argument')
        end
      elseif nargin == 2
        if (length(varargin{2}) == 1 & length(varargin{1}) <= 2) % check size
          self.T = abs(varargin{2});
          self.initial_state = [ varargin{1} 0 ];
        else 
          error('LongiFlatG:: bad constructor usage: wrong size of some argument')
        end
      else
        error('LongiFlatG:: bad constructor usage: too many input arguments')
      end

      self.core;

    end

  end

  

  methods (Access = protected)

    function core(self)
      % compute necessary quantities

      % coefficients of the polynomia. x = v_0*t.
      self.xCoefficients    = [ 0 0 0 0 self.initial_state(1) 0 ]; 
      self.final_state_data = [ self.initial_state(1) 0 ];

    end

  end

end
