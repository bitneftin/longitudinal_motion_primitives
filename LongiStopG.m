
% Class of primitives - Stop

%  #######################################################
%  #  _______   __  _____ ______ _______ _ ____   _____  #
%  #         \ |  |   ___|   ___|__   __| |    \ |       #
%  #          \|  |  __| |  __|    | |  | |     \|       #
%  #       |\     | |____| |       | |  | |   |\         #
%  #  _____| \____| _____|_|       |_|  |_|___| \______  #
%  #                                                     #
%  #######################################################

% Coded by Giammarco Valenti - version 2.0 
% /// KEEP IT SIMPLE! \\\

classdef LongiStopG < LongitudinalMotionPrimitive
  % Class for longitudinal free flow primitives
  % For what is a motion primitive refer to paper "Artificial Co-Drivers as an Enabling Technology for Future Intelligent 
  % Vehicles and Transportation Systems" M. Da Lio et all.
  % This class represents a primitive of motion in a "Stop" fashion. The final state is constrained only on Speed.
  % refer to "Biologically guided driver modelling: the stop behavior of human car drivers" Da Lio et all

  properties (SetAccess = protected, Hidden = false)
   % initial_state       % [ -   v_0 a_0 ] vector of initial states (vel & acc)    length = 2
   % final_state_data    % [ x_f  -   -  ] Position where to stop and final acc    length = 1
   wT                    % Time weight in the Lagrangian (wT + j^2) 
   wA                    % Final acceleration weight              
   % T                   % Time horizon
   xCoefficients         % coefficients of the primitive polynomial x(t) (in descent power order (MATLAB convention))
   af                    % optimal final acceleration
  end
    
  methods
    %% Constructor - Create a new primitive instance
    function self = LongiStopG( varargin )
      % Constructor for a primitive. 
      % Examples of usage:
      % Primitive = LongiStopG( [v_0 a_0] , [x_f])....................wT = 0.1 ; wA = 1;
      % Primitive = LongiStopG( [v_0 a_0] , [x_f], [wT]).........................wA = 1;
      % Primitive = LongiStopG( [v_0 a_0] , [x_f], [wT], [wA]) 


      % Default values

      self.wT = 0.1;
      self.wA = 1;


      % Input parsing
      % Size check for each ncase-argument 
      if     nargin < 2 
        error('LongiStopG:: bad constructor usage: Not enough input arguments: Put at least the initial state and the stop position! ([v0 a0],[xf])');

      elseif nargin == 2
        if(length(varargin{1}) ~= 2 || length(varargin{2}) ~= 1)
          error('LongiStopG:: bad constructor usage: Wrong size of some input');
        end
        if(varargin{2} <= 0)
          error('LongiStopG:: bad constructor usage: final position must be greater than 0');
        end
        self.initial_state       = varargin{1};      
        self.final_state_data    = varargin{2};

      elseif nargin == 3
        if(length(varargin{1}) ~= 2 || length(varargin{2}) ~= 1 || length(varargin{3}) ~= 1) % check size
          error('LongiStopG:: bad constructor usage: Wrong size of some input');
        end
        if(varargin{2} <= 0)
          error('LongiStopG:: bad constructor usage: final position must be greater than 0');
        end
        self.initial_state       = varargin{1};      
        self.final_state_data    = varargin{2};
        self.wT                  = varargin{3};

      elseif nargin == 4
        if(length(varargin{1}) ~= 2 || length(varargin{2}) ~= 1 || length(varargin{3}) ~= 1 || length(varargin{4}) ~= 1) % check size
          error('LongiStopG:: bad constructor usage: Wrong size of some input');
        end
        if(varargin{2} <= 0)
          error('LongiStopG:: bad constructor usage: final position must be greater than 0');
        end
        self.initial_state       = varargin{1};      
        self.final_state_data    = varargin{2};
        self.wT                  = varargin{3};
        self.wA                  = varargin{4};


      else
        error('LongiStopG:: bad constructor usage: too many input arguments \n');
      end

      self.core % execute the computations we need to define the whole primitive
                % This method "CORE" is computer at the construction stage
                % because otherwise no primitive is generated
    end
        
    %% Destructor - Destroy the class instance
    function delete(self)
      % Destructor
    end

    function jz = jZero(self)
      % return optimal jerk in 0
      jz = (60  *(self.final_state_data/self.T^3) - 36* (self.initial_state(1)/self.T^2) + 3*(self.af-3*self.initial_state(2))  / self.T) ;
    end

  end

  methods (Access = protected)
  function core(self) 

        % computes the main quantities we need.
        % Method which computes the primitive (coefficients of the polynomial and T).

        % Polynomial to solve to compute T - descent power order
        wa = self.wA;
        wt = self.wT;
        a0 = self.initial_state(2);
        v0 = self.initial_state(1);
        xf = self.final_state_data(1);
        
        polyT = [ wa*wt^2 , 18*wa*wt , 9*(9*wt - a0^2*wa^2) , - 144*a0*wa*(a0+v0*wa) ,...
         -72*(9*a0^2+8*v0^2*wa^2-5*a0*wa*(xf*wa - 6*v0)) , +144*(-7*a0*(9*v0-5*xf*wa) + 4*v0*wa*(-14*v0 + 5*xf*wa)),...
         +144*(135*a0*xf-216*v0^2+ 260*xf*v0*wa - 25*xf^2*wa^2),-43200*xf*(-3*v0+xf*wa),-129600*xf^2 ];

        Tlocal = FirstPositiveRoot(polyT);

        self.T = Tlocal;

        function fpr = FirstPositiveRoot(poly)
            allTs             = roots(poly);
            BoolInd           = (allTs > 0) & (imag(allTs)==0); % Get the real positive ones
            allTs(~BoolInd)   = inf;                            % Get rid of the garbage
            [fpr,~]           = min(allTs); 
        end

        % optimal final acceleration

        self.af = 3*(8*Tlocal*v0 + a0*Tlocal^2 - 20*xf)/(Tlocal^2*(9+Tlocal*wa));
        

        % x(t) primitive coefficients

        self.xCoefficients = ...
          [ (1/120)*(720 *(xf/Tlocal^5) - 360*(v0/Tlocal^4) + 60*(self.af-a0)   /(Tlocal^3)),...
            (1/24) *(-360*(xf/Tlocal^4) + 192*(v0/Tlocal^3) + 6*(6*a0-4*self.af)/(Tlocal^2)),...
            (1/6)  *(60  *(xf/Tlocal^3) - 36* (v0/Tlocal^2) + 3*(self.af-3*a0)  / Tlocal),...
            (1/2)  *a0,...
                    v0,...
            0 ];

    end
  end





end