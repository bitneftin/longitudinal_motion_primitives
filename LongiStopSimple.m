% Class of primitives - Stop - Time uncostrained (s_f dependent)

%  #######################################################
%  #  _______   __  _____ ______ _______ _ ____   _____  #
%  #         \ |  |   ___|   ___|__   __| |    \ |       #
%  #          \|  |  __| |  __|    | |  | |     \|       #
%  #       |\     | |____| |       | |  | |   |\         #
%  #  _____| \____| _____|_|       |_|  |_|___| \______  #
%  #                                                     #
%  #######################################################

% Coded by Giammarco Valenti % /// KEEP IT SIMPLE! \\\

classdef LongiStopSimple < LongitudinalMotionPrimitive
  % Class for longitudinal Go To At primitives
  % For what is a motion primitive refer to paper "Cooperative intersection support systems ... " M. Da Lio et all. 2018
  % This class represents a primitive of motion in a "Stop there" fashion:

  properties (SetAccess = protected, Hidden = false)
   % initial_state       % [ -  v_0  a_0 ] vector of initial states (vel & acc)    length = 2
   % final_state_data    % [ x_f ]
   % T                   % Time horizon
   xCoefficients         % coefficients of the primitive polynomial x(t) (in descent power order (MATLAB convention))
  end
    
  methods
    %% Constructor - Create a new primitive instance
    function self = LongiStopSimple( init , final )
      % Constructor of the primitive. Examples of usage:
      % Primitive = LongiStopSimple( [v_0 a_0] , [S_f] )


        if ( length(init) ~= 2 || (length(final) ~= 1) )
          error('LongiStopSimple:: bad constructor usage: Invalid input or size');
        end

        self.initial_state        = init;
        self.final_state_data     = final;


      %%------------------------------------------------------------------------
      self.core % execute the computations we need to define the whole primitive
                % This method "CORE" is computer at the construction stage
                % because otherwise no primitive is generated
      %%------------------------------------------------------------------------
    end
        
    %% Destructor - Destroy the class instance
    function delete(self)
      % Destructor
    end

    function jz = jZero(self)

      jz = self.xCoefficients(3) * 6;

    end


  end

  methods (Access = protected)
    function core(self) % computes the main quantities we need.

    % look at: cooperative intersection support system based on mirroring mechanism enacted...
    % (Appendix)
    % authors: Da Lio, Mazzalai , Darin

    % imporant values
    %T_      = self.final_state_data(2);
    s_f     = self.final_state_data(1);
    v_0     = self.initial_state(1);
    a_0     = self.initial_state(2);

    if s_f > ( (4*v_0^2) / (-5*a_0) ) && a_0 < 0
      % for positive acceleration it never happen
      s_f = (4*v_0^2) / (-5*a_0);
      self.final_state_data(1) = s_f;
    end

    T_      = 10 * s_f / ( 2*v_0 +  sqrt( 4*v_0^2 + 5*a_0*s_f ) );

    % coefficients
    c5      =  1/120 * ( + 720*(s_f / T_^5 ) - 360*(v_0 / T_^4) - 60*(a_0/T_^3) );
    c4      =  1/24  * ( - 360*(s_f / T_^4 ) + 192*(v_0 / T_^3) + 36*(a_0/T_^2) );
    c3      =  1/6   * ( +  60*(s_f / T_^3 ) -  36*(v_0 / T_^2) -  9*(a_0/T_^1) );
    c2      =  1/2   * a_0;
    c1      =          v_0;
    c0      =          0;


    % assign key properties
    self.xCoefficients = [ c5 c4 c3 c2 c1 c0 ];
    self.T  = T_;

    end

  end


end














