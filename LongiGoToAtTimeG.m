% Class of primitives - Go To At - Time constrained

%  #######################################################
%  #  _______   __  _____ ______ _______ _ ____   _____  #
%  #         \ |  |   ___|   ___|__   __| |    \ |       #
%  #          \|  |  __| |  __|    | |  | |     \|       #
%  #       |\     | |____| |       | |  | |   |\         #
%  #  _____| \____| _____|_|       |_|  |_|___| \______  #
%  #                                                     #
%  #######################################################

% Coded by Giammarco Valenti % /// KEEP IT SIMPLE! \\\

classdef LongiGoToAtTimeG < LongitudinalMotionPrimitive
  % Class for longitudinal Go To At primitives
  % For what is a motion primitive refer to paper "Artificial Co-Drivers as an Enabling Technology for Future Intelligent 
  % Vehicles and Transportation Systems" M. Da Lio et all.
  % This class represents a primitive of motion in a "Go there and at this Time" fashion:

  % Time and Position are GIVEN: this is useful for intersection support.

  properties (SetAccess = protected, Hidden = false)
   % initial_state       % [ -  v_0  a_0 ] vector of initial states (vel & acc)    length = 2
   % final_state_data    % [ x_f T ]
   % T                   % Time horizon
   xCoefficients         % coefficients of the primitive polynomial x(t) (in descent power order (MATLAB convention))
  end
    
  methods
    %% Constructor - Create a new primitive instance
    function self = LongiGoToAtTimeG( init , final )
      % Constructor of the primitive. Examples of usage:
      % Primitive = LongiGoToAtTimeG( [v_0 a_0] , [S0 T] )


        if ( length(init) ~= 2 || (length(final) ~= 2) )
          error('LongiGoToAtTimeG:: bad constructor usage: Invalid input or size');
        end

        self.initial_state        = init;
        self.final_state_data     = final;


      %%------------------------------------------------------------------------
      self.core % execute the computations we need to define the whole primitive
                % This method "CORE" is computer at the construction stage
                % because otherwise no primitive is generated
      %%------------------------------------------------------------------------
    end
        
    %% Destructor - Destroy the class instance
    function delete(self)
      % Destructor
    end

    function jz = jZero(self)

      jz = self.xCoefficients(3) * 6;

    end

    function v_f = vFinal( self )
      % final velocity of the primitive

      T_      = self.final_state_data(2);
      s_f     = self.final_state_data(1);
      v_0     = self.initial_state(1);
      a_0     = self.initial_state(2);
      
      v_f     = (15/8)*(s_f/T_) - (a_0*T_/8) - (7*v_0/8);

    end

    function [ V_star ] = vStar( self )
      % V star is present if the initial acceleration is negative
      % It is the minimum of the function v(T)

      T_      = self.final_state_data(2);
      s_f     = self.final_state_data(1);
      v_0     = self.initial_state(1);
      a_0     = self.initial_state(2);

      if a_0 < 0
        V_star     = 1/4 * ( -15 * a_0 * s_f )^(1/2) - 7/8 * v_0;
      else
        V_star     = inf; % return infinite in those cases
      end

    end

    function [ T_star ] = TStar( self )
      % V star is present if the initial acceleration is negative
      % It is the minimum of the function v(T)

      T_      = self.final_state_data(2);
      s_f     = self.final_state_data(1);
      v_0     = self.initial_state(1);
      a_0     = self.initial_state(2);

      if a_0 < 0
        T_star     =   sqrt( -15 * ( s_f / a_0 ) );
      else
        T_star     =   inf; % return infinite in those cases
      end

    end

    function [ jerkSet , new_extremal_primitives ] = JerkSetInBetween( self , second_primitive )
      % Given two primitives (of type LongiGoToAtTimeG), returns the jerkset between them.
      % Output is a 1x2 vector with minimum and maximum jerk
      % primitives must be generated with same IDENTICAL initial condition
      % Reference paper is not available: please refer to the author giammarco@rocketmail.com

      if ~isa(second_primitive,'LongiGoToAtTimeG')
        error('second primitive for the jerk set must be of the same time: LongiGoToAtTimeG');
      end
      % check the initial conditions
      if ~all(self.initial_state == second_primitive.initial_state)
        error('The two primitives must be generated with the same initial conditions!');
      end
      if self.final_state_data(1) ~= second_primitive.final_state_data(1)
        error('The two primitives must be generated with the same final distance');
      end

      
      % load all the parameters (from the first primitive, since they are the same for both the primitives)
      v_0 = self.initial_state(1);
      a_0 = self.initial_state(2);
      s_f = self.final_state_data(1);
      
      % Compute extremal point of the function ( d J_0(T) / dt ) (max two)
      % Check of the radical argument is positive
      radical_argument = 30*a_0*s_f + 25*v_0*v_0;

      % Initialize to the extremal with the monotonic J(T) case
      T_monotonic_case = sort([ self.T, second_primitive.T ]);
      T_ext            = T_monotonic_case;
      allTs            = T_ext;


      if radical_argument > 0

        radical = sqrt(radical_argument);

        T_ext(1) = ( -5*v_0  + radical ) ./ (2.*a_0);
        T_ext(2) = ( -5*v_0  - radical ) ./ (2.*a_0);
        
        % Verify if there are times into the interval
        
        if ~( T_ext(1) > T_monotonic_case(1) && T_ext(1) < T_monotonic_case(2) )
            T_ext(1) = T_monotonic_case(1); %re-assign if out of the interval
        end
        if ~( T_ext(2) > T_monotonic_case(1) && T_ext(2) < T_monotonic_case(2) )
            T_ext(2) = T_monotonic_case(2); %re-assign if out of the interval
        end

        % Compute jerk set taking into account the extremal points if present (union)
        % evaluate Jerk0(Text) and concatenate with the extremal primitives ones
        allJTs = [ 6*( -2.*a_0.*T_ext.^2 - 5.*v_0.*T_ext + 5*s_f )./(2.*T_ext.^3) , self.jZero , second_primitive.jZero ];

        jerkSet = [ min( allJTs ) max( allJTs ) ]; % union with possible extremas of jerk0(T) in [T1 T2]

        % returns the new extremal primitives if requested:
        if nargout == 2 % new extremal are the same primitives

          % compute extremal Ts (their index in Ts)
          Ts            = [ T_ext , self.T , second_primitive.T ];

          [ ~ , index_Max ] = max( [ allJTs , self.jZero , second_primitive.jZero ] ); % get argMax J(Ts(i)) 
          [ ~ , index_min ] = min( [ allJTs , self.jZero , second_primitive.jZero ] ); % get argmin J(Ts(i))

          % compute primitives with new Ts


          if nargout == 2 % new time-extremal primitives if requested
            new_extremal_primitives(1) = LongiGoToAtTimeG( self.initial_state , [s_f Ts(index_min)] ); % new primitive (recomputed)
            new_extremal_primitives(2) = LongiGoToAtTimeG( self.initial_state , [s_f Ts(index_Max)] ); % new primitive (recomputed)
          end

        end

      else

        % if no extremal Ts, J(T) is monotonic and then bounds are given
        jerkSet = sort([ self.jZero , second_primitive.jZero ]);

        % returns the new extremal primitives if requested,
        if nargout == 2 % new extremal are the same primitives
          new_extremal_primitives(1) = self;
          new_extremal_primitives(2) = second_primitive;
        end

      end

    end


  end

  methods (Access = protected)
    function core(self) % computes the main quantities we need.

    % look at: cooperative intersection support system based on mirroring mechanism enacted...
    % (Appendix)
    % authors: Da Lio, Mazzalai , Darin

    % imporant values
    T_      = self.final_state_data(2);
    s_f     = self.final_state_data(1);
    v_0     = self.initial_state(1);
    a_0     = self.initial_state(2);

    v_f     = (15/8)*(s_f/T_) - (a_0*T_/8) - (7*v_0/8);

    % coefficients
    c5      =  1/120 * ( + 720*(s_f/ T_^5 ) - 360*(v_0/T_^4) - 60*(a_0/T_^3) - 360*(v_f/T_^4) );
    c4      =  1/24  * ( - 360*(s_f/ T_^4 ) + 192*(v_0/T_^3) + 36*(a_0/T_^2) + 168*(v_f/T_^3) );
    c3      =  1/6   * ( +  60*(s_f/ T_^3 ) -  36*(v_0/T_^2) -  9*(a_0/T_^1) -  24*(v_f/T_^2) );
    c2      =  1/2   * a_0;
    c1      =          v_0;
    c0      =          0;


    % assign key properties
    self.xCoefficients = [ c5 c4 c3 c2 c1 c0 ];
    self.T  = T_;

    end

  end


end














