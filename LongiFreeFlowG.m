
% Class of primitives - Free flow

%  #######################################################
%  #  _______   __  _____ ______ _______ _ ____   _____  #
%  #         \ |  |   ___|   ___|__   __| |    \ |       #
%  #          \|  |  __| |  __|    | |  | |     \|       #
%  #       |\     | |____| |       | |  | |   |\         #
%  #  _____| \____| _____|_|       |_|  |_|___| \______  #
%  #                                                     #
%  #######################################################

% Coded by Giammarco Valenti - version 2.0
% /// KEEP IT SIMPLE! \\\

classdef LongiFreeFlowG < LongitudinalMotionPrimitive
  % Class for longitudinal free flow primitives
  % For what is a motion primitive refer to paper "Artificial Co-Drivers as an Enabling Technology for Future Intelligent 
  % Vehicles and Transportation Systems" M. Da Lio et all.
  % This class represents a primitive of motion in a "free flow" fashion. The final state is constrained only on Speed.

  properties (SetAccess = protected, Hidden = false)
  %  initial_state       % [ -   v_0 a_0 ] vector of initial states (vel & acc)    length = 2
  %  final_state_data    % [ -   v_f -   ] final velocity (final acc default is 0) length = 1
  wT                     % Time weight in the Lagrangian (wT + j^2)               
  %  T                   % Time horizon
  xCoefficients          % coefficients of the primitive polynomial x(t) (in descent power order (MATLAB convention))
  end
    
  methods
    %% Constructor - Create a new primitive instance
    function self = LongiFreeFlowG( varargin )
      % Constructor for a primitive. Examples of usage:
      % Primitive = LongiFreeFlowG( [v_0 a_0] )...............v_f = v_0 ; wT = 0.1
      % Primitive = LongiFreeFlowG( [v_0 a_0] , [v_f])....................wT = 0.1
      % Primitive = LongiFreeFlowG( [v_0 a_0] , [v_f], [wT])


      % Default values

      self.wT = 0.1;


      % Input parsing
      % Size check for each ncase-argument 

      if     nargin == 0 
        error('LongiFreeFlowG:: bad constructor usage: Not enough input arguments: Put at least the initial state! ([v0 a0])');

      elseif nargin == 1
        if(length(varargin{1}) ~= 2) % check size
          error('LongiFreeFlowG:: bad constructor usage: Wrong initial states size, it must be: ([v0 a0])');
        end
        self.initial_state       = varargin{1};
        self.final_state_data    = self.initial_state(1); % If no new velocity is desired, keep the initial one, the primitive is flat     (in my opinion it does not make a grinza)
        if(self.initial_state(2) == 0)
          self.initial_state(2) = 10*eps; % acceleration different from zero to avoid singularity
          warning('LongiFreeFlowG:: Acceleration is taken very small but not zero to avoid singularity')
        end

      elseif nargin == 2
        if(length(varargin{1}) ~= 2 || length(varargin{2}) ~= 1)
          error('LongiFreeFlowG:: bad constructor usage: Wrong size of some input');
        end
        self.initial_state       = varargin{1};      
        self.final_state_data = varargin{2};

        if(self.initial_state(2) == 0 && self.initial_state(1) == self.final_state_data)
          self.initial_state(2) = 10*eps; % acceleration different from zero to avoid singularity
          warning('LongiFreeFlowG:: Acceleration is taken very small but not zero to avoid singularity')
        end

      elseif nargin == 3
        if(length(varargin{1}) ~= 2 || length(varargin{2}) ~= 1 || length(varargin{3}) ~= 1) % check size
          error('LongiFreeFlowG:: bad constructor usage: Wrong size of some input');
        end
        self.initial_state       = varargin{1};      
        self.final_state_data    = varargin{2};
        self.wT                  = varargin{3};
      else
        error('LongiFreeFlowG:: bad constructor usage \n');
      end

      self.core % execute the computations we need to define the whole primitive
                % This method "CORE" is computer at the construction stage
                % because otherwise no primitive is generated
    end
        
    %% Destructor - Destroy the class instance
    function delete(self)
      % Destructor
    end

    % overloaded method to compute the jerk in zero
    function jZ = jZero(self) 
      % return jerk in zero
      jZ = 6*self.xCoefficients(2);
    end

  end

  methods (Access = protected)
    function core(self)
        % Method which computes the primitive (coefficients of the polynomial and T).

        % Assign initial condition and data

        A         = self.initial_state(2);
        U         = self.initial_state(1);
        Ufin      = self.final_state_data;
        wTlocal   = self.wT;

        % Computations of the coefficients


        DD1     =  (A.^2+6.*(U+(-1).*Ufin).*wTlocal.^(1/2)).^(1/2); % Two constant for repetitive patterns
        DD2     =  (A.^2+6.*((-1).*U+Ufin).*wTlocal.^(1/2)).^(1/2);
                  
        allTs    = [(A+(-1).*DD1).*wTlocal.^(-1/2),(A+DD1).*wTlocal.^(-1/2),((-1).*A+(-1).*DD2) ... %All the 4 possible T, select the least positive
                      .*wTlocal.^(-1/2),((-1).*A+DD2).*wTlocal.^(-1/2)];

        BoolInd           = (allTs > 0) & (imag(allTs)==0); % Get the real positive ones
        allTs(~BoolInd)   = inf;                            % Get rid of the garbage
        [Tlocal,TIndex]   = min(allTs);                     % Select the minimum positive T and its index


        self.T = Tlocal;

        computeCoeff(TIndex); % compute coefficients from T

        % SOLVE THE "impulsive solution problem"
        % If the jerk is out of the allowed range: the solution must be another one.

        jerkThreeshold = 6; % max jerk allowed (3 sigma human gaussian jerk)

        % check the jerk
          % 6*... is the jerk 

        solutionsTerminated = false; % it serves to span the solutions in case of high jerk

        while (abs(6*self.jZero) > jerkThreeshold) && ~solutionsTerminated;
          allTs(TIndex) = inf;                % Remove the actual T from the allTs array 
          [~,TIndex]    = min(allTs);          % Select the NEXT minimum positive T and its index             
          if allTs(TIndex) ~= inf             % Assign T only if it does exist
            Tlocal = allTs(TIndex);
            % re-assign T
            self.T = Tlocal;
            % re-compute the coefficients with the new T
            computeCoeff(TIndex);
          else
            solutionsTerminated = true;
            % warning('LongiFreeFlowG:: An high jerk solution is possible only');
          end
        end 

        function computeCoeff(TIndex)
          switch TIndex % select the right set of coefficient: use case structure for the presence of complex numbers
            % Code generated with Wolfram Mathematica
            case 1      
              self.xCoefficients = [(-1/4).*A.^2.*((-1).*A+DD1).*Tlocal.^(-4).*wTlocal.^(-1)+(1/4).* ...
                  A.*DD1.*((-1).*A+DD1).*Tlocal.^(-4).*wTlocal.^(-1)+(1/2).*(A+(-1) ...
                  .*DD1).*Tlocal.^(-4).*U.*wTlocal.^(-1/2)+(1/2).*((-1).*A+DD1).* ...
                  Tlocal.^(-4).*Ufin.*wTlocal.^(-1/2),(2/3).*A.^2.*((-1).*A+DD1).* ...
                  Tlocal.^(-3).*wTlocal.^(-1)+(-2/3).*A.*DD1.*((-1).*A+DD1).* ...
                  Tlocal.^(-3).*wTlocal.^(-1)+((-1).*A+DD1).*Tlocal.^(-3).*U.* ...
                  wTlocal.^(-1/2)+(A+(-1).*DD1).*Tlocal.^(-3).*Ufin.*wTlocal.^(-1/2) ...
                  ,(-1/2).*A.^2.*((-1).*A+DD1).*Tlocal.^(-2).*wTlocal.^(-1)+(1/2).* ...
                  A.*DD1.*((-1).*A+DD1).*Tlocal.^(-2).*wTlocal.^(-1),(A+(-1).*DD1).* ...
                  Tlocal.^(-1).*U.*wTlocal.^(-1/2),0];
            case 2
              self.xCoefficients =  [(1/4).*A.^2.*(A+DD1).*Tlocal.^( ...
                  -4).*wTlocal.^(-1)+(1/4).*A.*DD1.*(A+DD1).*Tlocal.^(-4).* ...
                  wTlocal.^(-1)+(1/2).*(A+DD1).*Tlocal.^(-4).*U.*wTlocal.^(-1/2)+( ...
                  1/2).*((-1).*A+(-1).*DD1).*Tlocal.^(-4).*Ufin.*wTlocal.^(-1/2),( ...
                  -2/3).*A.^2.*(A+DD1).*Tlocal.^(-3).*wTlocal.^(-1)+(-2/3).*A.*DD1.* ...
                  (A+DD1).*Tlocal.^(-3).*wTlocal.^(-1)+((-1).*A+(-1).*DD1).* ...
                  Tlocal.^(-3).*U.*wTlocal.^(-1/2)+(A+DD1).*Tlocal.^(-3).*Ufin.* ...
                  wTlocal.^(-1/2),(1/2).*A.^2.*(A+DD1).*Tlocal.^(-2).*wTlocal.^(-1)+ ...
                  (1/2).*A.*DD1.*(A+DD1).*Tlocal.^(-2).*wTlocal.^(-1),(A+DD1).* ...
                  Tlocal.^(-1).*U.*wTlocal.^(-1/2),0];
            case 3      
              self.xCoefficients =  [(1/4).*A.^2.*(A+DD2).*Tlocal.^( ...
                  -4).*wTlocal.^(-1)+(1/4).*A.*DD2.*(A+DD2).*Tlocal.^(-4).* ...
                  wTlocal.^(-1)+(1/2).*((-1).*A+(-1).*DD2).*Tlocal.^(-4).*U.* ...
                  wTlocal.^(-1/2)+(1/2).*(A+DD2).*Tlocal.^(-4).*Ufin.*wTlocal.^( ...
                  -1/2),(-2/3).*A.^2.*(A+DD2).*Tlocal.^(-3).*wTlocal.^(-1)+(-2/3).* ...
                  A.*DD2.*(A+DD2).*Tlocal.^(-3).*wTlocal.^(-1)+(A+DD2).*Tlocal.^(-3) ...
                  .*U.*wTlocal.^(-1/2)+((-1).*A+(-1).*DD2).*Tlocal.^(-3).*Ufin.* ...
                  wTlocal.^(-1/2),(1/2).*A.^2.*(A+DD2).*Tlocal.^(-2).*wTlocal.^(-1)+ ...
                  (1/2).*A.*DD2.*(A+DD2).*Tlocal.^(-2).*wTlocal.^(-1),((-1).*A+(-1) ...
                  .*DD2).*Tlocal.^(-1).*U.*wTlocal.^(-1/2),0];
            case 4     
              self.xCoefficients =...
                  [(-1/4).*A.^2.*((-1).*A+ ...
                  DD2).*Tlocal.^(-4).*wTlocal.^(-1)+(1/4).*A.*DD2.*((-1).*A+DD2).* ...
                  Tlocal.^(-4).*wTlocal.^(-1)+(1/2).*((-1).*A+DD2).*Tlocal.^(-4).* ...
                  U.*wTlocal.^(-1/2)+(1/2).*(A+(-1).*DD2).*Tlocal.^(-4).*Ufin.* ...
                  wTlocal.^(-1/2),(2/3).*A.^2.*((-1).*A+DD2).*Tlocal.^(-3).* ...
                  wTlocal.^(-1)+(-2/3).*A.*DD2.*((-1).*A+DD2).*Tlocal.^(-3).* ...
                  wTlocal.^(-1)+(A+(-1).*DD2).*Tlocal.^(-3).*U.*wTlocal.^(-1/2)+(( ...
                  -1).*A+DD2).*Tlocal.^(-3).*Ufin.*wTlocal.^(-1/2),(-1/2).*A.^2.*(( ...
                  -1).*A+DD2).*Tlocal.^(-2).*wTlocal.^(-1)+(1/2).*A.*DD2.*((-1).*A+ ...
                  DD2).*Tlocal.^(-2).*wTlocal.^(-1),((-1).*A+DD2).*Tlocal.^(-1).*U.* ...
                  wTlocal.^(-1/2),0];
            otherwise
                error('LongiFreeFlowG::Something went VERY wrong')
          end
        end % end function

      end

  end
end